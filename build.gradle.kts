plugins {
    java
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

repositories {
    jcenter()
}

dependencies {
    compile("org.apache.commons:commons-math3:3.6.1")
}

task("hello") {
    doLast {
        println("Hello world")
    }
}